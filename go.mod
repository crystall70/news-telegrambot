module bot

go 1.15

require (
	github.com/BurntSushi/toml v0.4.1
	github.com/go-redis/redis/v8 v8.11.4
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.5.1
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
