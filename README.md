# News Bot

<a href="https://t.me/gear_wrench_bot"><img src="https://img.shields.io/badge/Telegram-🤖%20@gear_wrench_bot-orange"></a>
[![Go](https://img.shields.io/badge/Go%20version-1.16.2-blue)](https://golang.org/)


This is a simple Telegram Bot which shows actual news on a popular russian resources

## Used technology

* [telegram-bot-api](https://github.com/go-telegram-bot-api/telegram-bot-api) - Golang bindings for the Telegram Bot API
* [Redis client for Golang](https://github.com/go-redis/redis) - persistent storage for storing users
* Docker and docker-compose - deployment bot in isolated container


## Features
* docker support

## Installation

1. Install golang
2. Start redis-server
3. Grab file `bot.example.toml` inside directiry `configs`, rename it to `bot.toml`.
   Change its values for your preference.
4. Run `make` command in the root directory to build binary file.
5. Start your bot with `./bot` command.

If you want supervisor support for autostart and other tasks:

1. open `telegrambot.conf` file in `init` directory, change relevant options to match yours and move it to `etc/supervisor/conf.d`
2. `sudo service supervisor start`

Or, alternatively, you can use Docker to setup and run an application.

Grab and change file `redis.conf` in `deploy/redis/config` directory and run `docker-compose build && docker-compose up -d` command in root dir

## In development:
* test coverage
