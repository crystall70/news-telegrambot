FROM golang:latest

WORKDIR /app
COPY . /app

RUN go env -w GO111MODULE=on
RUN chmod +rx scripts/run.sh && ./scripts/run.sh

CMD ["/app/bot"]