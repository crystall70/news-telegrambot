package bot

const (
	deleteMsg = "<b><i>🗑 удаление ...</i></b>"
	closeMsg = "✖️ закрыть"
	startMsg = "<i>выбери источник 👇</i>"
)

var Rss = map[string]string {
	"habr best": "https://habr.com/ru/rss/best/",
	"habr adm": "https://habr.com/ru/rss/flows/admin/top/weekly/?fl=ru",
	"habr career": "https://habr.com/ru/rss/hub/career/top/weekly/?fl=ru",
	"habr fin": "https://habr.com/ru/rss/hub/finance/top/weekly/?fl=ru",
	"habr python": "https://habr.com/ru/rss/hub/python/top/weekly/?fl=ru",
	"habr science": "https://habr.com/ru/rss/hub/popular_science/top/weekly/?fl=ru",
	"habr law": "https://habr.com/ru/rss/hub/business-laws/all/?fl=ru",
	"habr networks": "https://habr.com/ru/rss/hub/social_networks/all/?fl=ru",
	"habr it-comp": "https://habr.com/ru/rss/hub/itcompanies/top/weekly/?fl=ru",
	"habr develop": "https://habr.com/ru/rss/flows/develop/top/weekly/?fl=ru",
	"habr news": "https://habr.com/ru/rss/news/",
	"habr go": "https://habr.com/ru/rss/search/?q=go&order_by=date&target_type=posts&hl=ru&fl=ru",
	"habr devops": "https://habr.com/ru/rss/hub/devops/top/weekly/?fl=ru",
	"habr space": "https://habr.com/ru/rss/hub/space/top/monthly/?fl=ru",
	"habr games": "https://habr.com/ru/rss/hub/games/top/weekly/?fl=ru",
	"habr algo": "https://habr.com/ru/rss/hub/algorithms/all/?fl=ru",
	"habr ii": "https://habr.com/ru/rss/hub/artificial_intelligence/all/?fl=ru",
	"habr brain": "https://habr.com/ru/rss/hub/brain/all/?fl=ru",
	"habr hacks": "https://habr.com/ru/rss/hub/lifehacks/all/?fl=ru",

	"mediazona": "https://zona.media/rss",
	"mash": "https://mash.ru/rss",
	"ria": "https://ria.ru/export/rss2/archive/index.xml",
	"lenta": "https://lenta.ru/rss",
	"vsrap": "https://vsrap.ru/feed/",
	"popmech": "https://www.popmech.ru/out/public-all.xml",
	"N+1": "https://nplus1.ru/rss",
	"tproger": "https://tproger.ru/feed",
	"stopgame": "https://rss.stopgame.ru/rss_all.xml",
	"science": "https://new-science.ru/feed/",
	"knife": "https://thecode.media/news/feed/",
	"iz": "https://iz.ru/rss",
	"vedomosti": "https://vedomosti.ru/rss/articles",
	"tass": "https://tass.ru/rss/v2.xml",
	"rtech": "https://news.rambler.ru/rss/tech",
	"rpolitics": "https://news.rambler.ru/rss/politics",
	"rworld": "https://news.rambler.ru/rss/world",
	"tvrain": "https://tvrain.ru/export/rss/all.xml",
	"meduza": "https://meduza.io//rss/all",
	"echo": "https://echo.msk.ru/news.rss",
	"TJ": "https://tjournal.ru/rss/all",
	"hi-news": "https://hi-news.ru/feed",
	"igromania": "https://www.igromania.ru/rss/news.rss",
	"vesti": "https://www.vesti.ru/vesti.rss",
	"aif": "https://aif.ru/rss/all.php",
	"russia_today": "https://russian.rt.com/rss/",
	"rbk": "http://static.feed.rbc.ru/rbc/logical/footer/news.rss",
	"rgru": "https://rg.ru/xml/index.xml",
	"studio21": "https://studio21.ru/feed/",
	"sports": "https://www.sports.ru/rss/topnews.xml",
	"championat": "https://www.championat.com/rss/article/",
	"riamo": "https://riamo.ru/export/rss.xml",
	"fax": "https://www.interfax.ru/rss.asp",
	"cnews": "https://www.cnews.ru/inc/rss/news_top.xml",
}


