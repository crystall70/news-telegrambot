package store

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"strconv"
)

type Store struct {
	StoreConf 	*StoreConfig
	Rdb 		*redis.Client
	Ctx 		context.Context
}

func NewStore(sc *StoreConfig) *Store {
	return &Store{
		StoreConf: sc,
		Ctx: context.Background(),
	}
}

func (r *Store) AddUser(log *logrus.Logger, uid int64, username string) {
	getUser := r.Rdb.HMGet(r.Ctx, strconv.FormatInt(uid, 10), "username").Val()
	v := getUser[0]
	if v == nil {
		r.Rdb.HMSet(r.Ctx, strconv.FormatInt(uid, 10), "username", username, "start", 0, "close", 0)
		log.WithFields(logrus.Fields{
			"ID": uid,
			"username": username,
		}).Info("New user")
	}
}

func (r *Store) ProcessCommand(log *logrus.Logger, uid int64, key string) {
	r.Rdb.IncrBy(r.Ctx, "total", 1)
	val := r.Rdb.HMGet(r.Ctx, strconv.FormatInt(uid, 10), key).Val()
	if len(val) != 0 {
		v := val[0]
		var value int
		if v, ok := v.(string); ok {
			value, _ = strconv.Atoi(v)
			value += 1
		}
		r.Rdb.HMSet(r.Ctx, strconv.FormatInt(uid, 10), key,  value)

		log.WithFields(logrus.Fields{
			"ID": uid,
			fmt.Sprintf("%s", key): value,
		}).Debug()
	}
}

func (r *Store) ConnectDatabase() (*redis.Client, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", r.StoreConf.Host, r.StoreConf.Port),
		Password: r.StoreConf.Password, DB: 0,
	})
	_, err := rdb.Ping(r.Ctx).Result()
	if err != nil {
		return nil, err
	}
	return rdb, nil
}